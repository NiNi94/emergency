import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import routes from "./components/Routes";
import Login from "./components/Login";
import Signup from "./components/Signup";
import Settings from "./components/Settings";
import Navbar from "./components/Navbar";
function App() {
  return (
      <Router>
        <Navbar/>
          <Switch>
            <Route path={`/${routes.login.value}`} children={<Login/>}/>
            <Route path={`/${routes.signup.value}`} children={<Signup/>}/>
            <Route path={`/${routes.settings.value}`} children={<Settings/>}/>
          </Switch>
      </Router>
  );
}

export default App;
