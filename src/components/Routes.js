const routes = {
    login: {
        name: "Login",
        value: "login",
    },
    signup: {
        name: "Signup",
        value: "signup",
    },
    settings: {
        name: "Settings",
        value: "Settings",
    }
}

export default routes