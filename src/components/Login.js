import {useState} from 'react';
import "./css/Login.css";

function Login(props) {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    function onSubmit(e) {
        e.preventDefault()
        console.log(username)
        alert(`${username} ${password}`)
    }


    return (
        <form className={"flex-container"}>
            <div className={"flex-container-circle"}>
                <h2 className={"title"}>Login</h2>
                <div className={"input-field"}>
                    <label>Username: </label>
                    <input type="text" value={username} onChange={(event) => setUsername(event.target.value)}/>
                </div>
                <div className={"input-field"}>
                    <label>Password: </label>
                    <input type="password" value={password} onChange={(event) => setPassword(event.target.value)}/>
                </div>
                    <br/>
                <input className={"submit-button"} type="submit" value="Submit" onSubmit={onSubmit}/>
            </div>
        </form>
    );
}

export default Login;