import React from 'react';
import {Link} from "react-router-dom";

function Navbar(props) {
    return (
        <ul>
            <li>
                <Link to="/login">Login</Link>
            </li>
            <li>
                <Link to="/signup">Signup</Link>
            </li>
            <li>
                <Link to="/settings">Settings</Link>
            </li>
        </ul>
    );
}

export default Navbar;